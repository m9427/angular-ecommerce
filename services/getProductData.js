myApp.factory("getProductData", function () {
  return {
    requestProductData: function (id, $scope, $http) {
      $http.get(`https://fakestoreapi.com/products/${id}`).then(
        function (response) {

          $scope.productData = response.data;

          if ((response.data === "")) {
            $scope.blankAPIData = true;
            console.log("blank data");
          }

          $scope.blankAPIData = false;
          $scope.apiError = false;
        },
        function (err) {
          console.log(err);
          $scope.apiError = true;
        }
      );
    },
  };
});
