myApp.factory("getAPIData", function () {
  return {
    requestAPIData: function ($scope, $http) {
      $http.get("https://fakestoreapi.com/products").then(
        function (response) {
          $scope.APIData = response.data;
          if ((response.data === "")) {
            $scope.blankAPIData = true;
          }
          $scope.blankAPIData = false;
          $scope.apiError = false;
        },
        function (err) {
          console.log(err);
          $scope.apiError = true;
        }
      );
    },
  };
});
