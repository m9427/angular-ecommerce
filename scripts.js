let myApp = angular.module("myApp", ["ngRoute"]);

function manipulateHomepageData($scope, getProductData, getAPIData, $http) {
  function init() {
    getAPIData.requestAPIData($scope, $http);
  }
  init();
}

function manipulateProductPage($scope, getProductData, $http, $routeParams) {
  let myId = $routeParams["id"];
  function init(myParams) {
    // console.log(myParams);
    getProductData.requestProductData(myParams, $scope, $http);
  }
  init(myId);
}

myApp.controller("homepageController", manipulateHomepageData);
myApp.controller("productController", manipulateProductPage);

myApp.config(function ($routeProvider) {
  $routeProvider
    .when("/", {
      controller: "homepageController",
      templateUrl: "/components/displayCards.html",
      // template: '<h1> This is first </h1>'
    })
    .when("/:id", {
      controller: "productController",
      templateUrl: "/components/productPage.html",
      // template: '<h1> This is second </h1>'
    })
    .otherwise({
      redirectTo: "/",
    });
});
